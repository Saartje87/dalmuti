import { RoomActions } from './roomActions';
import { UserActions } from './userActions';

export type AppActions = UserActions | RoomActions;
