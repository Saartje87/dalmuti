import { User } from 'types/user';
import { createAction, createActionWithPayload } from './helpers';

export const storeUser = (user: User) => createActionWithPayload('@user/STORE_USER', user);

export const clearUser = () => createAction('@user/CLEAR');

export type UserActions = ReturnType<typeof storeUser | typeof clearUser>;
