import { createAction, createActionWithPayload } from './helpers';

interface JoinRoom {
  roomName: string;
  password?: string;
}

export const joinRoom = (payload: JoinRoom) => createActionWithPayload('@room/CREATE', payload);

export const leaveRoom = () => createAction('@room/LEAVE');

export type RoomActions = ReturnType<typeof joinRoom | typeof leaveRoom>;
