import { AppActions } from 'actions';
import { Reducer } from 'redux';
import { Room } from 'types/room';

export type RoomReducer = Partial<Room>;

const initialState: RoomReducer = {};

export const roomReducer: Reducer<RoomReducer, AppActions> = (state = initialState, action) => {
  switch (action.type) {
    case '@user/CLEAR':
    case '@room/LEAVE':
      return initialState;

    case '@room/CREATE':
      return {
        ...state,
        ...action.payload,
      };

    default:
      return state;
  }
};
