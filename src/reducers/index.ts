import { RoomReducer } from './room';
import { UserReducer } from './user';

export { roomReducer as room } from './room';
export { userReducer as user } from './user';

export type AppState = {
  user: UserReducer;
  room: RoomReducer;
};
