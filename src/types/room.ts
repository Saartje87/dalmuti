export interface Room {
  roomName: string;
  password?: string;
}
