export interface ApiCompany {
  id: number;
  name: string;
  chamberOfCommerceNumber: string;
  streetName: string;
  houseNumber: number;
  postalCode: string;
  appointmentTypes: {
    id: number;
    title: string;
    description: string;
    duration: number;
  }[];
  employees: {
    id: number;
    name: string;
    title: string;
    function: string;
  }[];
  locations: {
    id: number;
    name: string;
    address: string;
  }[];
}
