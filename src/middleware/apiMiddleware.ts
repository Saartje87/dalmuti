import { Action, Dispatch, Middleware, MiddlewareAPI } from 'redux';

interface ApiAction<R> extends Action {
  payload?: any;
  meta?: {
    api?: {
      method: 'GET' | 'PUT' | 'POST' | 'PATCH' | 'DELETE';
      path: string;
      onSuccess?: (dispatch: Dispatch, response: R) => void;
      onError?: (dispatch: Dispatch, error: Error) => void;
    };
  };
}

const defetch = async (
  method: 'GET' | 'PUT' | 'POST' | 'PATCH' | 'DELETE',
  url: string,
  token: string,
  body?: any,
) => {
  const headers: Record<string, string> = token
    ? { 'Content-Type': 'application/json', 'X-API-TOKEN': token }
    : { 'Content-Type': 'application/json' };
  const options: RequestInit = {
    mode: 'cors',
    cache: 'no-cache',
    credentials: 'same-origin',
    headers,
    redirect: 'follow',
    referrer: 'no-referrer',
    method,
  };

  if (body) {
    options.body = JSON.stringify(body);
  }

  const response = await fetch(url, options);

  const result = await response.json();
  const { status } = response;

  if (!response.ok) {
    throw new Error(result.error);
  }

  if (status < 200 || status >= 300) {
    throw new Error(`Invalid status code ${status}`);
  }

  return result;
};

interface Options {
  baseUrl: string;
}

export function api({ baseUrl }: Options) {
  const apiMiddleware: Middleware = ({ getState, dispatch }: MiddlewareAPI) => (
    next: Dispatch,
  ) => async (action: ApiAction<any>) => {
    const state = getState();
    // eslint-disable-next-line @typescript-eslint/camelcase
    const { api_token } = state.user;
    const returnValue = next(action);
    const { meta } = action;

    if (meta && meta.api) {
      const { payload } = action;
      const { method, path, onSuccess, onError } = meta.api;

      await defetch(method, baseUrl + path, api_token, payload)
        .then(response => onSuccess && onSuccess(dispatch, response))
        .catch(error => {
          if (onError) {
            onError(dispatch, error);
          }

          throw error;
        });
    }

    return returnValue;
  };

  return apiMiddleware;
}
