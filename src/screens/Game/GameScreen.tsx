import { Box, Button, Heading } from '@blockle/blocks';
import { useSelector } from '@blockle/react-redux';
import React from 'react';
import { AppState } from 'reducers';

const GameScreen = () => {
  const room = useSelector((state: AppState) => state.room);

  return (
    <>
      <Box paddingX="gutter" paddingY="small">
        <Heading level={1}>{room.roomName}</Heading>
      </Box>
      <Box flexGrow={1} paddingX="gutter">
        <Heading level={2}>Table</Heading>
      </Box>
      <Box flexGrow={1} paddingX="gutter">
        <Heading level={2}>Your cards</Heading>
      </Box>
      <Box paddingX="gutter" paddingY="small" display="flex" justifyContent="space-between">
        <Button>Pass</Button>
        <Button>Play selection</Button>
      </Box>
    </>
  );
};

export default GameScreen;
