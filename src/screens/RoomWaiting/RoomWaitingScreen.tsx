import { Box, Button, Dialog, FlatButton, Heading } from '@blockle/blocks';
import { useDispatch, useSelector } from '@blockle/react-redux';
import { leaveRoom } from 'actions/roomActions';
import React, { useState } from 'react';
import { AppState } from 'reducers';

const RoomWaitingScreen = () => {
  const dispatch = useDispatch();
  const room = useSelector((state: AppState) => state.room);
  const [leaveDialog, setLeaveDialog] = useState(false);

  return (
    <>
      <Box paddingX="gutter" paddingY="small">
        <Heading level={1}>{room.roomName}</Heading>
      </Box>
      <Box flexGrow={1} paddingX="gutter">
        PLAYERS
      </Box>
      <Box paddingX="gutter" paddingY="small" display="flex" justifyContent="flex-end">
        <Button onClick={() => setLeaveDialog(true)}>Leave</Button>
      </Box>

      <Dialog
        open={leaveDialog}
        onRequestClose={() => setLeaveDialog(false)}
        render={() => <></>}
        title="Leave room?"
        actions={
          <>
            <FlatButton onClick={() => dispatch(leaveRoom())}>Back</FlatButton>
            <FlatButton onClick={() => dispatch(leaveRoom())}>Leave</FlatButton>
          </>
        }
      />
    </>
  );
};

export default RoomWaitingScreen;
