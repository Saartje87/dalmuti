import { Paper } from '@blockle/blocks';
import { useSelector } from '@blockle/react-redux';
import Layout from 'components/Layout';
import RouteTransition from 'components/RouteTransition';
import ServiceWorker from 'components/ServiceWorker';
import React from 'react';
import { hot } from 'react-hot-loader';
import { AppState } from 'reducers';
import CreateRoomScreen from 'screens/CreateRoom/CreateRoomScreen';
import LobbyScreen from 'screens/Lobby';
import RoomScreen from 'screens/Room';
import WelcomeScreen from 'screens/Welcome';
import { isAuthenticated } from 'selectors/userSelectors';
import './root-screen.css';

const RootScreen = () => {
  const authenticated = useSelector(isAuthenticated);
  const joinedRoom = useSelector((state: AppState) => !!state.room.roomName);

  return (
    <>
      <Paper
        open={authenticated}
        fit
        render={() => (
          <>
            <Layout>
              {joinedRoom ? <RoomScreen /> : <LobbyScreen />}

              <RouteTransition path="/create-room" exact effect="slideUp">
                <CreateRoomScreen />
              </RouteTransition>
            </Layout>
          </>
        )}
      />

      <Paper
        open={!authenticated}
        fit
        render={() => (
          <>
            <Layout>
              <WelcomeScreen />
            </Layout>
          </>
        )}
      />

      <ServiceWorker />
    </>
  );
};

export default hot(module)(RootScreen);
