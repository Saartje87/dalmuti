import React from 'react';
import GameScreen from 'screens/Game';
import RoomWaitingScreen from 'screens/RoomWaiting';

const RoomScreen = () => {
  const gameStarted = true;

  if (gameStarted) {
    return <GameScreen />;
  }

  return <RoomWaitingScreen />;
};

export default RoomScreen;
