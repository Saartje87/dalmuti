import { Box, Button, Heading, Stack, TextField } from '@blockle/blocks';
import { useHistory } from '@blockle/router';
import React from 'react';

const LobbyScreen = () => {
  const { push } = useHistory();

  return (
    <>
      <Box paddingX="gutter" paddingY="small">
        <Heading level={1} textAlign="center">
          Lobby
        </Heading>
      </Box>
      <Box overflow="auto" flexGrow={1} paddingX="gutter">
        <Stack space="small">
          <Heading level={2}>Rooms</Heading>
          <TextField name="roomName" label="Search room" onChange={value => console.log(value)} />
        </Stack>
      </Box>
      <Box display="flex" justifyContent="flex-end" paddingX="gutter" paddingY="small">
        <Button onClick={() => push('/create-room')}>Create Room</Button>
      </Box>
    </>
  );
};

export default LobbyScreen;
