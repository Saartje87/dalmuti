import { Box, Button, Heading, Stack, TextField } from '@blockle/blocks';
import { Form } from '@blockle/form';
import { useDispatch } from '@blockle/react-redux';
import { useHistory } from '@blockle/router';
import { joinRoom } from 'actions/roomActions';
import React from 'react';

interface FormData {
  roomName: string;
  password?: string;
}

const CreateRoomScreen = () => {
  const { goBack, replace } = useHistory();
  const dispatch = useDispatch();
  const onSubmit = (formData: FormData) => {
    dispatch(joinRoom(formData));
    replace('/');
  };

  return (
    <Box paddingX="gutter" paddingY="small">
      <Stack space="small">
        <Heading level={1}>Create room</Heading>
        <Form
          onSubmit={onSubmit}
          render={() => (
            <>
              <TextField name="roomName" label="Room name" required />
              <TextField name="password" label="Password" type="password" />

              <Box display="flex" justifyContent="space-between">
                <Button onClick={goBack}>Back</Button>
                <Button type="submit">Create Room</Button>
              </Box>
            </>
          )}
        />
      </Stack>
    </Box>
  );
};

export default CreateRoomScreen;
