import { Box, Button, TextField } from '@blockle/blocks';
import { Form } from '@blockle/form';
import { useDispatch } from '@blockle/react-redux';
import { storeUser } from 'actions/userActions';
import React from 'react';

interface FormData {
  username: string;
}

const WelcomeScreen = () => {
  const dispatch = useDispatch();
  const onSubmit = (formData: FormData) => {
    dispatch(
      storeUser({
        // TODO
        id: Math.floor(Math.random() * 255e9).toString(32),
        ...formData,
      }),
    );
  };

  return (
    <Box display="flex" flexGrow={1} alignItems="center" justifyContent="center" padding="gutter">
      <Box width="full">
        <Form
          onSubmit={onSubmit}
          render={() => (
            <Box width="full">
              <TextField name="username" label="Username" required />
              <Button type="submit">Go</Button>
            </Box>
          )}
        />
      </Box>
    </Box>
  );
};

export default WelcomeScreen;
