import { Box, cx, Icon, Paper, Ripple } from '@blockle/blocks';
import React, { ReactElement } from 'react';
import './stepper.css';
import StepperItem from './StepperItem';

interface Props {
  children: React.ReactNode;
  index: number;
  requestIndexChange: (index: number) => void;
}

const Stepper = ({ children, index, requestIndexChange }: Props) => {
  const nodes = React.Children.toArray(children);
  const elements = nodes.filter(node =>
    React.isValidElement<ReturnType<typeof StepperItem>>(node),
  ) as ReactElement<Parameters<typeof StepperItem>[0]>[];

  return (
    <Box display="flex" flexDirection="column" flexGrow={1} paddingX="gutter" paddingTop="gutter">
      <Box className="Stepper-Items" flexGrow={1} position="relative">
        {elements.map(({ props }, elementIndex) => (
          <Paper key={elementIndex} fit open={elementIndex === index} render={props.render} />
        ))}
      </Box>
      <Box display="flex" flexShrink={0}>
        {elements.map(({ props }, elementIndex) => (
          <Ripple
            key={elementIndex}
            component="button"
            className={cx('Stepper-Button', elementIndex <= index && 'is-active')}
            onClick={() => {
              if (elementIndex <= index) {
                requestIndexChange(elementIndex);
              }
            }}
          >
            <div className={`Stepper-Button-Icon ${elementIndex <= index ? 'is-active' : ''}`}>
              {elementIndex < index ? (
                <Icon size="small" name="check" label="Stepper Item" />
              ) : (
                elementIndex + 1
              )}
            </div>
            <span className="Stepper-Button-Label">{props.label}</span>
          </Ripple>
        ))}
      </Box>
    </Box>
  );
};

export default Stepper;
