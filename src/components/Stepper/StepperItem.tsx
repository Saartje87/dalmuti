import React from 'react';

interface Props {
  label: React.ReactNode | string;
  render: () => JSX.Element | string;
  disabled?: boolean;
}

const StepperItem = ({}: Props) => null;

export default StepperItem;
