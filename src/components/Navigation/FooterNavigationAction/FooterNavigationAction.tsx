import { Icon, IconNames, Ripple, Text } from '@blockle/blocks';
import { Route } from '@blockle/router';
import React from 'react';
import './footer-navigation-action.css';

interface Props {
  icon: IconNames;
  label: string;
  to: string;
}

const FooterNavigationAction = ({ icon, label, to }: Props) => (
  <Route
    path={to}
    exact={to === '/'}
    render={match => (
      <Ripple
        component="a"
        href={`#${to}`}
        className={`FooterNavigationAction ${match ? 'is-active' : ''}`}
      >
        <Icon name={icon} label={label} color={match ? 'primary' : 'gray'} />
        <Text fontSize="xsmall">{label}</Text>
      </Ripple>
    )}
  />
);

export default FooterNavigationAction;
