import { Paper, PaperEffects } from '@blockle/blocks';
import { Route } from '@blockle/router';
import Layout from 'components/Layout';
import React from 'react';

type Props = {
  children: React.ReactNode;
  path: string | string[];
  exact?: boolean;
  effect?: PaperEffects;
};

const RouteTransition = ({ children, exact = false, path, effect = 'fade' }: Props) => {
  return (
    <Route
      path={path}
      exact={exact}
      render={match => (
        <Paper
          fit
          open={match}
          effect={effect}
          render={() => (
            <>
              <Layout fit>{children}</Layout>
            </>
          )}
        />
      )}
    />
  );
};

export default RouteTransition;
