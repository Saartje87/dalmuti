import { Card, Loader, Stack, Text } from '@blockle/blocks';
import { useSelector } from '@blockle/react-redux';
import { useHistory } from '@blockle/router';
import React, { useCallback } from 'react';
import { AppState } from 'reducers';
import { getFullAppointmentDetails } from 'selectors/appointmentSelectors';

interface Props {
  appointmentId: string;
}

const AppointmentCard = ({ appointmentId }: Props) => {
  const { push } = useHistory();
  const selector = useCallback(
    (state: AppState) => getFullAppointmentDetails(state, appointmentId),
    [appointmentId],
  );
  const appointment = useSelector(selector);

  if (!appointment) {
    return <Card shadow="1">{appointmentId} not found</Card>;
  }

  // Waiting for data..
  if (!appointment.appointmentType || !appointment.location || !appointment.employee) {
    return <Loader />;
  }

  const startDate = new Date(appointment.startDateTime);

  return (
    <Card shadow="1" onClick={() => push(`/appointment/${appointment.id}`)}>
      <Stack space="small">
        <Text color="black">{appointment.appointmentType.description}</Text>
        <Stack space="xsmall">
          <Text fontSize="small">
            {startDate.toDateString()} at {startDate.toLocaleTimeString().substr(0, 5)}
          </Text>
          <Text>{appointment.location.name}</Text>
          <Text>{appointment.location.address}</Text>
        </Stack>
      </Stack>
    </Card>
  );
};

export default AppointmentCard;
