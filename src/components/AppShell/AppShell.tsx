import React from 'react';
import { Router } from '@blockle/router';
import { Provider } from '@blockle/react-redux';
import { createHashHistory } from 'history';

import { store, persistor } from 'config/store';
import { PersistGate } from 'redux-persist/integration/react';

type Props = {
  children: React.ReactNode;
};

const AppShell = ({ children }: Props) => {
  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <Router history={createHashHistory()}>{children}</Router>
      </PersistGate>
    </Provider>
  );
};

export default AppShell;
