import { Icon, Ripple } from '@blockle/blocks';
import { FieldProps, useField } from '@blockle/form';
import React from 'react';
import './checkbox.css';

interface Props extends FieldProps<string> {
  checked?: boolean;
  children: React.ReactNode;
  name: string;
  onChange?: (value: boolean) => void;
  required?: boolean;
}

const Checkbox = ({ children, checked, name, required }: Props) => {
  const { value, setValue } = useField<boolean>(name, {
    value: !!checked,
    validate: (value: boolean) => {
      return required && !value ? 'required' : null;
    },
  });

  return (
    <div className="Checkbox-Container">
      <Ripple component="div" className="Checkbox" onClick={() => setValue(!value)}>
        <div
          className={`Checkbox-Marker ${value ? 'is-checked' : ''}`}
          role="checkbox"
          aria-checked={value}
          tabIndex={0}
          // aria-labelledby={this.id}
          onKeyUp={event => {
            if (event.keyCode === 32) {
              setValue(!value);
            }
          }}
        >
          <Icon name="check" label="Toggle checkbox" size="small" />
        </div>
      </Ripple>
      <label
        // id={this.id}
        className="Checkbox-Label"
        onClick={() => setValue(!value)}
      >
        {children}
      </label>
    </div>
  );
};

Checkbox.defaultProps = {
  checked: false,
};

export default Checkbox;
