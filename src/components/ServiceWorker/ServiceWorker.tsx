import { Box, FlatButton, Heading, Panel, Stack } from '@blockle/blocks';
import React, { useEffect, useState } from 'react';
import { Workbox } from 'workbox-window';

type State = 'idle' | 'activated' | 'waiting';

const ServiceWorker = () => {
  const [state, setState] = useState<State>('idle');
  const reload = () => window.location.reload();

  useEffect(() => {
    if (!('serviceWorker' in navigator) || module.hot) {
      return;
    }

    const wb = new Workbox('/sw.js');

    wb.addEventListener('activated', event => {
      if (event.isUpdate) {
        setState('activated');
      }
    });

    wb.addEventListener('waiting', () => {
      setState('waiting');
    });

    wb.register();
  }, []);

  if (state === 'activated') {
    return (
      <Panel
        size="s"
        open
        onRequestClose={reload}
        render={() => (
          <Box paddingX="gutter" paddingTop="large">
            <Stack space="large">
              <Heading level={2} fontSize="large">
                Update found
              </Heading>
              <FlatButton onClick={reload}>Install</FlatButton>
            </Stack>
          </Box>
        )}
      />
    );
  }

  if (state === 'waiting') {
    console.log('Waiting for sw update');
  }

  return null;
};

export default ServiceWorker;
