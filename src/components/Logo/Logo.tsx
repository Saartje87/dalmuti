import React from 'react';

import './logo.css';
import LogoPNG from 'public/images/logo.png';

interface Props {
  size?: 'small' | 'medium' | 'large';
}

const Logo = ({ size = 'medium' }: Props) => (
  <img className={`logo size-${size}`} src={LogoPNG} alt="Blockle logo" />
);

export default Logo;
