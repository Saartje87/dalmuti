import React, { Component } from 'react';

interface Props {
  [index: string]: any;
  render(props: State): React.ReactNode;
}

interface State {
  [index: string]: any;
}

/**
 * TODO Find a better suitable name..?
 *
 * Reminds all props values passed to this component and pass them to render function.
 *
 * Usefull for animating elements that could be gone in the leaving animation.
 *
 * <Memorize
 *  id={params.id}
 *  render={({ id}) => <MyElement id={id} />}
 * >
 */
export default class Memorize extends Component<Props, State> {
  static getDerivedStateFromProps(props: Props, state: State) {
    const nextState = { ...state };

    Object.keys(props).forEach(key => {
      if (props[key] !== undefined) {
        nextState[key] = props[key];
      }
    });

    return nextState;
  }

  state = {};

  render() {
    const { render } = this.props;

    return render(this.state);
  }
}
