import React, { useState, useRef, useEffect } from 'react';

type RenderParams = {
  index: number;
  style: React.CSSProperties;
  value: any;
};

type Props = {
  render: (config: RenderParams) => JSX.Element;
  style?: React.CSSProperties;
  className?: string;
  getRowByIndex: (index: number) => any;
  itemCount?: number;
};

const ForeverScroll = ({ render, getRowByIndex, itemCount = 10, ...props }: Props) => {
  const [index, setIndex] = useState(0);
  const container = useRef<HTMLDivElement>(null);
  const map = new Array(itemCount).fill(null);

  return (
    <div ref={container} {...props}>
      {map.map((v, index) =>
        render({
          index,
          style: { color: 'green' },
          value: getRowByIndex(index),
        }),
      )}
    </div>
  );
};

export default ForeverScroll;
