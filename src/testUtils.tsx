import '@testing-library/jest-dom/extend-expect';
import React from 'react';
import { Provider } from '@blockle/react-redux';
import { Router } from '@blockle/router';
import { render as testRender } from '@testing-library/react';
import configureStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import { createMemoryHistory } from 'history';

const mockStore = configureStore([thunk]);

export * from '@testing-library/react';

export const render = (Component: React.ReactElement<any>, initialState = {}) => {
  const store = mockStore(initialState);
  const setup = (Component: React.ReactElement<any>) => (
    <Router key="router" history={createMemoryHistory()}>
      <Provider key="store" store={store}>
        {Component}
      </Provider>
    </Router>
  );
  const renderUtils = testRender(setup(Component));
  const { container, baseElement } = renderUtils;

  return {
    store,
    ...renderUtils,
    rerender: (Component: React.ReactElement<any>) => {
      testRender(setup(Component), { container, baseElement });
    },
  };
};
