interface Window {
  __REDUX_DEVTOOLS_EXTENSION_COMPOSE__?: Function;
}

declare namespace NodeJS {
  export interface ProcessEnv {
    API: string;
  }
}
