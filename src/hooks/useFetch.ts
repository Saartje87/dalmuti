import { useSelector } from '@blockle/react-redux';
import { api } from 'lib/api';
import { useEffect, useState } from 'react';
import { AppState } from 'reducers';

type State<R> = {
  data: R | undefined;
  loading: boolean;
  error: boolean;
};

export const useFetch = <R>(url: string): State<R> => {
  const apiToken = useSelector((state: AppState) => state.user.api_token);
  const [state, setState] = useState<State<R>>({
    data: undefined,
    loading: true,
    error: false,
  });
  const load = async () => {
    setState({
      data: undefined,
      loading: true,
      error: false,
    });

    try {
      const data = await api.get<R>(url, apiToken);

      setState({
        data,
        loading: false,
        error: false,
      });
    } catch (error) {
      setState({
        data: undefined,
        loading: false,
        error: true,
      });
    }
  };

  useEffect(() => {
    load();
  }, [url]);

  return state;
};
