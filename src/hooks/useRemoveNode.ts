import { useEffect } from 'react';

export const useRemoveNode = (query: string, when: boolean) => {
  useEffect(() => {
    const node = document.querySelector(query);

    if (!node) {
      if (process.env.NODE_ENV !== 'production') {
        console.warn(`useRemoveNode: ${query} not found`);
      }
      return;
    }

    node.parentNode!.removeChild(node);
  }, [when]);
};

export const removeNode = node => {};
