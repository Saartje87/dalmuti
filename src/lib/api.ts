const baseUrl = process.env.API;

const fetchConfig: RequestInit = {
  mode: 'cors',
  cache: 'no-cache',
  credentials: 'same-origin',
  headers: {
    'Content-Type': 'application/json',
  },
  redirect: 'follow',
  referrer: 'no-referrer',
};

export const api = {
  async get<R = any>(url: string, token?: string): Promise<R> {
    const options: RequestInit = {
      headers: {
        'Content-Type': 'application/json',
        'X-API-TOKEN': token || '',
      },
    };
    const response = await fetch(baseUrl + url, options);
    const result = await response.json();
    const { status } = response;

    if (!response.ok) {
      throw new Error(result.error);
    }

    if (status < 200 || status >= 300) {
      throw new Error(`Invalid status code ${status}`);
    }

    return result;
  },
  async post<R = any, D = any>(url: string, data: D): Promise<R> {
    const response = await fetch(baseUrl + url, {
      ...fetchConfig,
      method: 'POST',
      body: JSON.stringify(data),
    });
    const { status } = response;
    const result = await response.json();

    if (!response.ok) {
      throw new Error(result.error);
    }

    if (status < 200 || status >= 300) {
      throw new Error(`Invalid status code ${status}`);
    }

    return result;
  },
  async put<R = any, D = any>(url: string, data: D): Promise<R> {
    const response = await fetch(baseUrl + url, {
      ...fetchConfig,
      method: 'PUT',
      body: JSON.stringify(data),
    });
    const { status } = response;
    const result = await response.json();

    if (!response.ok) {
      throw new Error(result.error);
    }

    if (status < 200 || status >= 300) {
      throw new Error(`Invalid status code ${status}`);
    }

    return result;
  },
  async patch<R = any, D = any>(url: string, data: D): Promise<R> {
    const response = await fetch(baseUrl + url, {
      ...fetchConfig,
      method: 'PATCH',
      body: JSON.stringify(data),
    });
    const { status } = response;
    const result = await response.json();

    if (!response.ok) {
      throw new Error(result.error);
    }

    if (status < 200 || status >= 300) {
      throw new Error(`Invalid status code ${status}`);
    }

    return result;
  },
  async delete(url: string) {
    const response = await fetch(baseUrl + url, {
      ...fetchConfig,
      method: 'DELETE',
    });
    const { status } = response;
    const result = await response.json();

    if (!response.ok) {
      throw new Error(result.error);
    }

    if (status < 200 || status >= 300) {
      throw new Error(`Invalid status code ${status}`);
    }
  },
};

export type Api = typeof api;
