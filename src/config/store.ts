import { AppActions } from 'actions';
import { api as apiMiddleware } from 'middleware/apiMiddleware';
import * as reducers from 'reducers';
import { AppState } from 'reducers'; // tslint:disable-line no-duplicate-imports
import { applyMiddleware, combineReducers, compose, createStore, Store } from 'redux';
import { PersistConfig, persistReducer, persistStore } from 'redux-persist';
import { PersistPartial } from 'redux-persist/es/persistReducer';
import storage from 'redux-persist/es/storage';
import thunk from 'redux-thunk';

const persistConfig: PersistConfig<AppState> = {
  key: 'blockle-appoinment',
  storage,
  whitelist: ['user', 'room'],
};

const devToolEnhancer =
  process.env.NODE_ENV !== 'production' && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
    ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__()
    : (x: any) => x;

const enhancer = compose(
  // Use `thunk.withExtraArgument(api)`
  applyMiddleware(apiMiddleware({ baseUrl: process.env.API }), thunk),
  devToolEnhancer,
);

const rootReducer = combineReducers({
  ...reducers,
});

const persistedReducer = persistReducer(persistConfig, rootReducer);

export const store: Store<AppState> = createStore<AppState & PersistPartial, AppActions, any, void>(
  persistedReducer,
  enhancer,
);
export const persistor = persistStore(store);

if (module.hot) {
  module.hot.accept('../reducers', () => {
    const nextRootReducer = require('../reducers');

    store.replaceReducer(nextRootReducer);
  });
}
