import { AppState } from '../reducers';

export const getLocations = (state: AppState) => Object.values(state.locations);

export const getLocation = (state: AppState, locationId: string) => state.locations[locationId];
