import { AppState } from '../reducers';

export const getAppointmentTypes = (state: AppState) => Object.values(state.appointmentTypes);

export const getAppointmentType = (state: AppState, appointmentTypeId: string) =>
  state.appointmentTypes[appointmentTypeId];
