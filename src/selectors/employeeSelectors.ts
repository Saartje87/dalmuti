import { AppState } from '../reducers';

export const getEmployees = (state: AppState) => Object.values(state.employees);

export const getEmployee = (state: AppState, employeeId: string) => state.employees[employeeId];
