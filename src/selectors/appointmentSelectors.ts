import { Appointment } from 'reducers/appointment';
import { AppState } from '../reducers';
import { getAppointmentType } from './appointmentTypeSelectors';
import { getEmployee } from './employeeSelectors';
import { getLocation } from './locationSelectors';

export const getAppointments = (state: AppState) => Object.values<Appointment>(state.appointments);

export const getAppointment = (state: AppState, appointmentId: string) =>
  state.appointments[appointmentId];

export const getFullAppointmentDetails = (state: AppState, appointmentId: string) => {
  const appointment = getAppointment(state, appointmentId);

  if (!appointment) {
    return undefined;
  }

  return {
    id: appointment.id,
    startDateTime: appointment.startDateTime,
    stopDateTime: appointment.stopDateTime,
    appointmentType: getAppointmentType(state, appointment.appointmentTypeId),
    employee: getEmployee(state, appointment.employeeId),
    location: getLocation(state, appointment.locationId),
  };
};
