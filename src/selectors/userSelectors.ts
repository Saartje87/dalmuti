import { AppState } from 'reducers';

export const isAuthenticated = (state: AppState) => !!state.user.id;
