const webpack = require('webpack');
const Dotenv = require('dotenv-webpack');

const { config, PATHS } = require('./webpack-base.config');

module.exports = {
  ...config,
  entry: {
    app: ['react-hot-loader/patch', './main'],
  },
  devtool: 'eval-source-map',
  devServer: {
    contentBase: PATHS.public,
    disableHostCheck: true,
    hot: true,
    noInfo: false,
    host: '0.0.0.0',
    port: 8000,
  },
  module: {
    ...config.module,
    rules: [
      ...config.module.rules,
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader?sourceMap', 'postcss-loader'],
      },
      {
        test: /\.jsx?$/,
        include: /node_modules/,
        use: ['react-hot-loader/webpack'],
      },
    ],
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NamedModulesPlugin(),
    new Dotenv({
      path: './.env.local',
    }),
    ...config.plugins,
  ],
};
