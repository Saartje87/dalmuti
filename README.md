# Dalmuti

## Setup

```bash
yarn
```

## Build

```bash
yarn build:production
yarn build:develop
```

## Test

```bash
yarn test
yarn lint
```
